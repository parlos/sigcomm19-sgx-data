%% Data generated from giveOVSData.sh
%% Remove comments as needed.

%Common defs. 
LW=2;
sf=1e6;
bins=[0:1e-6:12e-3];

%%%%%%%%%%%%%%
%% First part. for the first incoming packet
%% Data used pre 20181120
% % SGX64=importdata('33211.txt');
% % SGX128=importdata('33212.txt');
% % SGX256=importdata('33213.txt');
% % SGX512=importdata('33214.txt');
% % SGX1024=importdata('33215.txt'); 
% % VAN64=importdata('33229.txt');
% % VAN128=importdata('33230.txt');
% % VAN256=importdata('33231.txt');
% % VAN512=importdata('33232.txt');
% % VAN1024=importdata('33233.txt');

%data for after 20181120, jorge re-ran experiments
SGX64=importdata('33958.txt');
SGX128=importdata('33959.txt');
SGX256=importdata('33960.txt');
SGX512=importdata('33961.txt');
SGX1024=importdata('33962.txt'); 
VAN64=importdata('33932.txt');
VAN128=importdata('33933.txt');
VAN256=importdata('33935.txt');
VAN512=importdata('33937.txt');
VAN1024=importdata('33938.txt');


fprintf('|Packet Size| |SGX | | | OVS_vanilla | |\n')
fprintf('|Packet Size|Mean|Stdev|min|max || Mean|Stdev|min|max |\n')
fprintf('64 SGX     & %.0f & %.0f & %.0f & %.0f \\\\ \n',sf*mean(SGX64(:,3)),   sf*std(SGX64(:,3)),    sf*min(SGX64(:,3)),    sf*max(SGX64(:,3)) )
fprintf('64 Plain   & %.0f & %.0f & %.0f & %.0f \\\\ \n',sf*mean(VAN64(:,3)),       sf*std(VAN64(:,3)),    sf*min(VAN64(:,3)),    sf*max(VAN64(:,3)))
fprintf('\\emph{\\textbf{Overhead}} & %g \\%% & \\multicolumn{3}{|c|}{} \\\\ \n', (mean(SGX64(:,3))-mean(VAN64(:,3)))/mean(VAN64(:,3)))

fprintf('128 SGX    & %.0f & %.0f & %.0f & %.0f \\\\ \n',sf*mean(SGX128(:,3)),  sf*std(SGX128(:,3)),   sf*min(SGX128(:,3)),   sf*max(SGX128(:,3)) )
fprintf('128 Plain  & %.0f & %.0f & %.0f & %.0f \\\\ \n',sf*mean(VAN128(:,3)),      sf*std(VAN128(:,3)),   sf*min(VAN128(:,3)),   sf*max(VAN128(:,3)))
fprintf('\\emph{\\textbf{Overhead}} & %g \\%% & \\multicolumn{3}{|c|}{} \\\\ \n', (mean(SGX128(:,3))-mean(VAN128(:,3)))/mean(VAN128(:,3)))

fprintf('256 SGX    & %.0f & %.0f & %.0f & %.0f \\\\ \n',sf*mean(SGX256(:,3)),  sf*std(SGX256(:,3)),   sf*min(SGX256(:,3)),   sf*max(SGX256(:,3)))
fprintf('256 Plain  & %.0f & %.0f & %.0f & %.0f \\\\ \n',sf*mean(VAN256(:,3)),      sf*std(VAN256(:,3)),   sf*min(VAN256(:,3)),   sf*max(VAN256(:,3)))
fprintf('\\emph{\\textbf{Overhead}} & %g \\%% & \\multicolumn{3}{|c|}{} \\\\ \n', (mean(SGX256(:,3))-mean(VAN256(:,3)))/mean(VAN256(:,3)))

fprintf('512 SGX    & %.0f & %.0f & %.0f & %.0f \\\\ \n',sf*mean(SGX512(:,3)),  sf*std(SGX512(:,3)),   sf*min(SGX512(:,3)),   sf*max(SGX512(:,3)))
fprintf('512 Plain  & %.0f & %.0f & %.0f & %.0f \\\\ \n',sf*mean(VAN512(:,3)),      sf*std(VAN512(:,3)),   sf*min(VAN512(:,3)),   sf*max(VAN512(:,3)))
fprintf('\\emph{\\textbf{Overhead}} & %g \\%% & \\multicolumn{3}{|c|}{} \\\\ \n', (mean(SGX512(:,3))-mean(VAN512(:,3)))/mean(VAN512(:,3)))

fprintf('1024 SGX   & %.0f & %.0f & %.0f & %.0f \\\\ \n',sf*mean(SGX1024(:,3)), sf*std(SGX1024(:,3)),  sf*min(SGX1024(:,3)),  sf*max(SGX1024(:,3)))
fprintf('1024 Plain & %.0f & %.0f & %.0f & %.0f \\\\ \n',sf*mean(VAN1024(:,3)),     sf*std(VAN1024(:,3)),  sf*min(VAN1024(:,3)),  sf*max(VAN1024(:,3))) 
fprintf('\\emph{\\textbf{Overhead}} & %g \\%% & \\multicolumn{3}{|c|}{} \\\\ \n', (mean(SGX1024(:,3))-mean(VAN1024(:,3)))/mean(VAN1024(:,3)))



s1=histc(SGX64(:,3),bins);cdf_s1=cumsum(s1)/sum(s1);
s2=histc(SGX128(:,3),bins);cdf_s2=cumsum(s2)/sum(s2);
s3=histc(SGX256(:,3),bins);cdf_s3=cumsum(s3)/sum(s3);
s4=histc(SGX512(:,3),bins);cdf_s4=cumsum(s4)/sum(s4);
s5=histc(SGX1024(:,3),bins);cdf_s5=cumsum(s5)/sum(s5);

v1=histc(VAN64(:,3),bins);cdf_v1=cumsum(v1)/sum(v1);
v2=histc(VAN128(:,3),bins);cdf_v2=cumsum(v2)/sum(v2);
v3=histc(VAN256(:,3),bins);cdf_v3=cumsum(v3)/sum(v3);
v4=histc(VAN512(:,3),bins);cdf_v4=cumsum(v4)/sum(v4);
v5=histc(VAN1024(:,3),bins);cdf_v5=cumsum(v5)/sum(v5);


loglog(sf*bins,cdf_s1,'-r',...
      sf*bins,cdf_s2,'-r',...
      sf*bins,cdf_s3,'-r',...
      sf*bins,cdf_s4,'-r',...
      sf*bins,cdf_s5,'-r',...
      sf*bins,cdf_v1,':b',...
      sf*bins,cdf_v2,':b',...
      sf*bins,cdf_v3,':b',...
      sf*bins,cdf_v4,':b',...
      sf*bins,cdf_v5,':b','LineWidth',LW)
  
  
legend('SGX64','SGX128','SGX256','SGX512','SGX1024','Plain64','Plain128','Plain256','Plain512','Plain1024')
xlabel('Delay x [\mus]');
ylabel('Pr(X\leqx)')
axis([1000 8000 0.002 1.1]) 
%title('For the first incoming packet, (remove this title)')

print('../images/cdf_first_packet.pdf','-dpdf');

disp('press to cont');
pause

%%%%%%%%%%%%%%%%%
%% Second part, OVS has a rule in the flow tables. 
% % data pre 20181120
% % vanOr64_10=importdata('33217.txt');
% % vanOr64_100=importdata('33218.txt');
% % vanOr64_1000=importdata('33219.txt');
% % vanOr64_10000=importdata('33220.txt');
% % vanOr750_10=importdata('33221.txt');
% % vanOr750_100=importdata('33222.txt');
% % vanOr750_1000=importdata('33223.txt');
% % vanOr750_10000=importdata('33224.txt');
% % vanOr1024_10=importdata('33225.txt');
% % vanOr1024_100=importdata('33226.txt');
% % vanOr1024_1000=importdata('33227.txt');
% % vanOr1024_10000=importdata('33228.txt');
% % 
% % 
% % sgxOr64_10=importdata('33235.txt');
% % sgxOr64_100=importdata('33236.txt');
% % sgxOr64_1000=importdata('33237.txt');
% % sgxOr64_10000=importdata('33238.txt');
% % sgxOr750_10=importdata('33239.txt');
% % sgxOr750_100=importdata('33240.txt');
% % sgxOr750_1000=importdata('33241.txt');
% % sgxOr750_10000=importdata('33242.txt');
% % sgxOr1024_10=importdata('33243.txt');
% % sgxOr1024_100=importdata('33244.txt');
% % sgxOr1024_1000=importdata('33245.txt');
% % sgxOr1024_10000=importdata('33246.txt');



%% Post 20181120
vanOr64_10=importdata('33939.txt');
vanOr64_100=importdata('33944.txt');
vanOr64_1000=importdata('33945.txt');
vanOr64_10000=importdata('33946.txt');
vanOr750_10=importdata('33948.txt');
vanOr750_100=importdata('33949.txt');
vanOr750_1000=importdata('33950.txt');
vanOr750_10000=importdata('33951.txt');
vanOr1024_10=importdata('33952.txt');
vanOr1024_100=importdata('33953.txt');
vanOr1024_1000=importdata('33954.txt');
vanOr1024_10000=importdata('33955.txt');


sgxOr64_10=importdata('33964.txt');
sgxOr64_100=importdata('33965.txt');
sgxOr64_1000=importdata('33966.txt');
sgxOr64_10000=importdata('33967.txt');
sgxOr750_10=importdata('33968.txt');
sgxOr750_100=importdata('33969.txt');
sgxOr750_1000=importdata('33970.txt');
sgxOr750_10000=importdata('33971.txt');
sgxOr1024_10=importdata('33972.txt');
sgxOr1024_100=importdata('33973.txt');
sgxOr1024_1000=importdata('33974.txt');
sgxOr1024_10000=importdata('33975.txt');

%  plot(vanOr64_10(:,2),1000*vanOr64_10(:,3), ...
%      vanOr64_100(:,2),1000*vanOr64_100(:,3), ...
%      vanOr64_1000(:,2),1000*vanOr64_1000(:,3), ...
%      vanOr64_10000(:,2),1000*vanOr64_10000(:,3), ...
%      vanOr750_10(:,2),1000*vanOr750_10(:,3), ...
%      vanOr750_100(:,2),1000*vanOr750_100(:,3), ...
%      vanOr750_1000(:,2),1000*vanOr750_1000(:,3), ...
%      vanOr750_10000(:,2),1000*vanOr750_10000(:,3), ...
%      vanOr1024_10(:,2),1000*vanOr1024_10(:,3), ...
%      vanOr1024_100(:,2),1000*vanOr1024_100(:,3), ...
%      vanOr1024_1000(:,2),1000*vanOr1024_1000(:,3), ...
%      vanOr1024_10000(:,2),1000*vanOr1024_10000(:,3))

 
 
 
s1=histc(sgxOr64_10(:,3),bins);cdf_s1=cumsum(s1)/sum(s1);
s2=histc(sgxOr750_10(:,3),bins);cdf_s2=cumsum(s2)/sum(s2);
s3=histc(sgxOr1024_10(:,3),bins);cdf_s3=cumsum(s3)/sum(s3);

v1=histc(vanOr64_10(:,3),bins);cdf_v1=cumsum(v1)/sum(v1);
v2=histc(vanOr750_10(:,3),bins);cdf_v2=cumsum(v2)/sum(v2);
v3=histc(vanOr1024_10(:,3),bins);cdf_v3=cumsum(v3)/sum(v3);

% loglog(bins,cdf_s1,'-+r',...
%       bins,cdf_s2,'-sr',...
%       bins,cdf_s3,'-dr',...
%       bins,cdf_v1,':+b',...
%       bins,cdf_v2,':sb',...
%       bins,cdf_v3,':db','LineWidth',LW)
%  legend('SGX 64  10 \mu s','SGX 750 10 \mu s','SGX 1024  10 \mu s',...
%         'VAN 64  10 \mu s','VAN 750 10 \mu s','VAN 1024  10 \mu s')
% 

    
  
s1a=histc(sgxOr64_100(:,3),bins);cdf_s1a=cumsum(s1a)/sum(s1a);
s2a=histc(sgxOr750_100(:,3),bins);cdf_s2a=cumsum(s2a)/sum(s2a);
s3a=histc(sgxOr1024_100(:,3),bins);cdf_s3a=cumsum(s3a)/sum(s3a);

v1a=histc(vanOr64_100(:,3),bins);cdf_v1a=cumsum(v1a)/sum(v1a);
v2a=histc(vanOr750_100(:,3),bins);cdf_v2a=cumsum(v2a)/sum(v2a);
v3a=histc(vanOr1024_100(:,3),bins);cdf_v3a=cumsum(v3a)/sum(v3a);

% loglog(bins,cdf_s1a,'-+r',...
%       bins,cdf_s2a,'-sr',...
%       bins,cdf_s3a,'-dr',...
%       bins,cdf_v1a,':+b',...
%       bins,cdf_v2a,':sb',...
%       bins,cdf_v3a,':db','LineWidth',LW)
%  legend('SGX 64  100 \mu s','SGX 750 100 \mu s','SGX 1024  100 \mu s',...
%         'VAN 64  100 \mu s','VAN 750 100 \mu s','VAN 1024  100 \mu s')   
    
    
s1b=histc(sgxOr64_1000(:,3),bins);cdf_s1b=cumsum(s1b)/sum(s1b);
s2b=histc(sgxOr750_1000(:,3),bins);cdf_s2b=cumsum(s2b)/sum(s2b);
s3b=histc(sgxOr1024_1000(:,3),bins);cdf_s3b=cumsum(s3b)/sum(s3b);

v1b=histc(vanOr64_1000(:,3),bins);cdf_v1b=cumsum(v1b)/sum(v1b);
v2b=histc(vanOr750_1000(:,3),bins);cdf_v2b=cumsum(v2b)/sum(v2b);
v3b=histc(vanOr1024_1000(:,3),bins);cdf_v3b=cumsum(v3b)/sum(v3b);

% loglog(bins,cdf_s1b,'-+r',...
%       bins,cdf_s2b,'-sr',...
%       bins,cdf_s3b,'-dr',...
%       bins,cdf_v1b,':+b',...
%       bins,cdf_v2b,':sb',...
%       bins,cdf_v2b,':db','LineWidth',LW)
%  legend('SGX 64  1000 \mu s','SGX 750 1000 \mu s','SGX 1024  1000 \mu s',...
%         'VAN 64  1000 \mu s','VAN 750 1000 \mu s','VAN 1024  1000 \mu s')   
%     
      
s1c=histc(sgxOr64_10000(:,3),bins);cdf_s1c=cumsum(s1c)/sum(s1c);
s2c=histc(sgxOr750_10000(:,3),bins);cdf_s2c=cumsum(s2c)/sum(s2c);
s3c=histc(sgxOr1024_10000(:,3),bins);cdf_s3c=cumsum(s3c)/sum(s3c);

v1c=histc(vanOr64_10000(:,3),bins);cdf_v1c=cumsum(v1c)/sum(v1c);
v2c=histc(vanOr750_10000(:,3),bins);cdf_v2c=cumsum(v2c)/sum(v2c);
v3c=histc(vanOr1024_10000(:,3),bins);cdf_v3c=cumsum(v3c)/sum(v3c);

% loglog(bins,cdf_s1c,'-+r',...
%       bins,cdf_s2c,'-sr',...
%       bins,cdf_s3c,'-dr',...
%       bins,cdf_v1c,':+b',...
%       bins,cdf_v2c,':sb',...
%       bins,cdf_v2c,':db','LineWidth',LW)
%  legend('SGX 64  10000 \mu s','SGX 750 10000 \mu s','SGX 1024  10000 \mu s',...
%         'VAN 64  10000 \mu s','VAN 750 10000 \mu s','VAN 1024  10000 \mu s')   
%     

s1=sgxOr64_10(:,3);
s2=sgxOr750_10(:,3);
s3=sgxOr1024_10(:,3);
v1=vanOr64_10(:,3);
v2=vanOr750_10(:,3);
v3=vanOr1024_10(:,3);

s1a=sgxOr64_100(:,3);
s2a=sgxOr750_100(:,3);
s3a=sgxOr1024_100(:,3);
v1a=vanOr64_100(:,3);
v2a=vanOr750_100(:,3);
v3a=vanOr1024_100(:,3);

s1b=sgxOr64_1000(:,3);
s2b=sgxOr750_1000(:,3);
s3b=sgxOr1024_1000(:,3);
v1b=vanOr64_1000(:,3);
v2b=vanOr750_1000(:,3);
v3b=vanOr1024_1000(:,3);

s1c=sgxOr64_10000(:,3);
s2c=sgxOr750_10000(:,3);
s3c=sgxOr1024_10000(:,3);
v1c=vanOr64_10000(:,3);
v2c=vanOr750_10000(:,3);
v3c=vanOr1024_10000(:,3);


% 
% fprintf('|Packet Size| |SGX | | | OVS_vanilla | |\n')
% fprintf('|Packet Size|Mean|Stdev|min|max || Mean|Stdev|min|max |\n')
% fprintf('|  64-   10|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s1),    sf*std(s1),     sf*min(s1),     sf*max(s1),     sf*mean(v1),        sf*std(v1),     sf*min(v1),     sf*max(v1))
% fprintf('|  64-  100|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s1a),   sf*std(s1a),    sf*min(s1a),    sf*max(s1a),    sf*mean(v1a),       sf*std(v1a),    sf*min(v1a),    sf*max(v1a))
% fprintf('|  64- 1000|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s1b),   sf*std(s1b),    sf*min(s1b),    sf*max(s1b),    sf*mean(v1b),       sf*std(v1b),    sf*min(v1b),    sf*max(v1b))
% fprintf('|  64-10000|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s1c),   sf*std(s1c),    sf*min(s1c),    sf*max(s1c),    sf*mean(v1c),       sf*std(v1c),    sf*min(v1c),    sf*max(v1c))
% fprintf('| 750-   10|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s2),    sf*std(s2),     sf*min(s2),     sf*max(s2),     sf*mean(v2),        sf*std(v2),     sf*min(v2),     sf*max(v2))
% fprintf('| 750-  100|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s2a),   sf*std(s2a),    sf*min(s2a),    sf*max(s2a),    sf*mean(v2a),       sf*std(v2a),    sf*min(v2a),    sf*max(v2a))
% fprintf('| 750- 1000|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s2b),   sf*std(s2b),    sf*min(s2b),    sf*max(s2b),    sf*mean(v2b),       sf*std(v2b),    sf*min(v2b),    sf*max(v2b))
% fprintf('| 750-10000|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s2c),   sf*std(s2c),    sf*min(s2c),    sf*max(s2c),    sf*mean(v2c),       sf*std(v2c),    sf*min(v2c),    sf*max(v2c))
% fprintf('|1024-   10|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s3),    sf*std(s3),     sf*min(s3),     sf*max(s3),     sf*mean(v3),        sf*std(v3),     sf*min(v3),     sf*max(v3))
% fprintf('|1024-  100|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s3a),   sf*std(s3a),    sf*min(s3a),    sf*max(s3a),    sf*mean(v3a),       sf*std(v3a),    sf*min(v3a),    sf*max(v3a))
% fprintf('|1024- 1000|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s3b),   sf*std(s3b),    sf*min(s3b),    sf*max(s3b),    sf*mean(v3b),       sf*std(v3b),    sf*min(v3b),    sf*max(v3b))
% fprintf('|1024-10000|%g | %g  | %g | %g ||%g | %g |%g | %g |\n',sf*mean(s3c),   sf*std(s3c),    sf*min(s3c),    sf*max(s3c),    sf*mean(v3c),       sf*std(v3c),    sf*min(v3c),    sf*max(v3c))

disp('ver2')

fprintf('|Packet Size| |SGX | | | OVS_vanilla | |\n')
fprintf('|Packet Size|Mean|Stdev|min|max || Mean|Stdev|min|max |\n')
fprintf(' 10 $\\mu s$ 64 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',     sf*mean(s1),    sf*std(s1),     sf*min(s1),     sf*max(s1))
fprintf(' 10 $\\mu s$ 64 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',   sf*mean(v1),    sf*std(v1),     sf*min(v1),     sf*max(v1))
fprintf(' 10 $\\mu s$ 750 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',     sf*mean(s2),    sf*std(s2),     sf*min(s2),     sf*max(s2))
fprintf(' 10 $\\mu s$ 750 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',   sf*mean(v2),    sf*std(v2),     sf*min(v2),     sf*max(v2))
fprintf(' 10 $\\mu s$ 1024 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',     sf*mean(s3),    sf*std(s3),     sf*min(s3),     sf*max(s3))
fprintf(' 10 $\\mu s$ 1024 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',   sf*mean(v3),    sf*std(v3),     sf*min(v3),     sf*max(v3))
fprintf('\\hline \n')
fprintf(' 100 $\\mu s$ 64 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',    sf*mean(s1a),    sf*std(s1a),     sf*min(s1a),     sf*max(s1a))
fprintf(' 100 $\\mu s$ 64 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',  sf*mean(v1a),    sf*std(v1a),     sf*min(v1a),     sf*max(v1a))
fprintf(' 100 $\\mu s$ 750 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',    sf*mean(s2a),    sf*std(s2a),     sf*min(s2a),     sf*max(s2a))
fprintf(' 100 $\\mu s$ 750 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',  sf*mean(v2a),    sf*std(v2a),     sf*min(v2a),     sf*max(v2a))
fprintf(' 100 $\\mu s$ 1024 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',    sf*mean(s3a),    sf*std(s3a),     sf*min(s3a),     sf*max(s3a))
fprintf(' 100 $\\mu s$ 1024 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',  sf*mean(v3a),    sf*std(v3a),     sf*min(v3a),     sf*max(v3a))
fprintf('\\hline \n')
fprintf(' 1000 $\\mu s$ 64 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',   sf*mean(s1b),    sf*std(s1b),     sf*min(s1b),     sf*max(s1b))
fprintf(' 1000 $\\mu s$ 64 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n', sf*mean(v1b),    sf*std(v1b),     sf*min(v1b),     sf*max(v1b))
fprintf(' 1000 $\\mu s$ 750 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',   sf*mean(s2b),    sf*std(s2b),     sf*min(s2b),     sf*max(s2b))
fprintf(' 1000 $\\mu s$ 750 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n', sf*mean(v2b),    sf*std(v2b),     sf*min(v2b),     sf*max(v2b))
fprintf(' 1000 $\\mu s$ 1024 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',   sf*mean(s3b),    sf*std(s3b),     sf*min(s3b),     sf*max(s3b))
fprintf(' 1000 $\\mu s$ 1024 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n', sf*mean(v3b),    sf*std(v3b),     sf*min(v3b),     sf*max(v3b))
fprintf('\\hline \n')
fprintf(' 10000 $\\mu s$ 64 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',  sf*mean(s1c),    sf*std(s1c),     sf*min(s1c),     sf*max(s1c))
fprintf(' 10000 $\\mu s$ 64 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',sf*mean(v1c),    sf*std(v1c),     sf*min(v1c),     sf*max(v1c))
fprintf(' 10000 $\\mu s$ 750 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',  sf*mean(s2c),    sf*std(s2c),     sf*min(s2c),     sf*max(s2c))
fprintf(' 10000 $\\mu s$ 750 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',sf*mean(v2c),    sf*std(v2c),     sf*min(v2c),     sf*max(v2c))
fprintf(' 10000 $\\mu s$ 1024 bytes (SGX) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',  sf*mean(s3c),    sf*std(s3c),     sf*min(s3c),     sf*max(s3c))
fprintf(' 10000 $\\mu s$ 1024 bytes (Plain) & %.0f & %.0f  & %.0f & %.0f \\\\ \n',sf*mean(v3c),    sf*std(v3c),     sf*min(v3c),     sf*max(v3c))

% subplot(3,1,1),semilogx([100 1000 10000],[sf*mean(s1a) sf*mean(s1b) sf*mean(s1c)],... 
%                     [100 1000 10000],[sf*mean(s2a) sf*mean(s2b) sf*mean(s2c)],...
%                     [100 1000 10000],[sf*mean(s3a) sf*mean(s3b) sf*mean(s3c)],... 
%                     [100 1000 10000],[sf*mean(v1a) sf*mean(v1b) sf*mean(v1c)],... 
%                     [100 1000 10000],[sf*mean(v2a) sf*mean(v2b) sf*mean(v2c)],...
%                     [100 1000 10000],[sf*mean(v3a) sf*mean(v3b) sf*mean(v3c)],'LineWidth',LW);
%  legend('SGX 64','SGX 750','SGX 1024','VAN 64','VAN 750','VAN 1024') 
%  xlabel('IFG [us]');ylabel('Average Delay [\mu s]');
% 
%  subplot(3,1,2),semilogx([100 1000 10000],[sf*std(s1a) sf*std(s1b) sf*std(s1c)],... 
%                     [100 1000 10000],[sf*std(s2a) sf*std(s2b) sf*std(s2c)],...
%                     [100 1000 10000],[sf*std(s3a) sf*std(s3b) sf*std(s3c)],... 
%                     [100 1000 10000],[sf*std(v1a) sf*std(v1b) sf*std(v1c)],... 
%                     [100 1000 10000],[sf*std(v2a) sf*std(v2b) sf*std(v2c)],...
%                     [100 1000 10000],[sf*std(v3a) sf*std(v3b) sf*std(v3c)],'LineWidth',LW);
%  legend('SGX 64','SGX 750','SGX 1024','VAN 64','VAN 750','VAN 1024')   
%  xlabel('IFG [us]');ylabel('Std.dev [\mu s]');
%  
 
 barvalues=[mean(s1a) mean(v1a)  mean(s2a) mean(v2a) mean(s3a) mean(v3a)
            mean(s1b) mean(v1b)  mean(s2b) mean(v2b) mean(s3b) mean(v3b)
            mean(s1c) mean(v1c)  mean(s2c) mean(v2c) mean(s3c) mean(v3c)];
        
% subplot(3,1,3),bar(sf*barvalues)        
% legend('SGX 64','SGX 750','SGX 1024','VAN 64','VAN 750','VAN 1024');
% ylabel('Average Delay [ms]');
 
  errsvalues=[std(s1a) std(v1a) std(s2a) std(v2a) std(s3a) std(v3a)
             std(s1b)  std(v1b) std(s2b) std(v2b) std(s3b) std(v3b)
             std(s1c)  std(v1c) std(s2c) std(v2c) std(s3c) std(v3c)];
%  
%  barnames=['64 bytes','750 bytes','1024 bytes'];       
%  bb=bar(barvalues');
%  hold on
%  for i=1:3
%       x= get(get(bb(i),'children'),'xdata');
%       barsx(1:3,i)=mean(x,1);
%  end
% hold all 
%  er=errorbar(barsx,barvalues,barnames)

%figure
% 
% semilogx([100 1000 10000],[sf*mean(s1a) sf*mean(s1b) sf*mean(s1c)],... 
%          [100 1000 10000],[sf*mean(s2a) sf*mean(s2b) sf*mean(s2c)],...
%          [100 1000 10000],[sf*mean(s3a) sf*mean(s3b) sf*mean(s3c)],... 
%          [100 1000 10000],[sf*mean(v1a) sf*mean(v1b) sf*mean(v1c)],... 
%          [100 1000 10000],[sf*mean(v2a) sf*mean(v2b) sf*mean(v2c)],...
%          [100 1000 10000],[sf*mean(v3a) sf*mean(v3b) sf*mean(v3c)],'LineWidth',LW);
%  legend('SGX 64','SGX 750','SGX 1024','VAN 64','VAN 750','VAN 1024') 
%  xlabel('IFG [us]');ylabel('Average Delay [\mu s]');

barwitherr(sf*errsvalues,sf*barvalues)        
legend('SGX 64','Plain 64','SGX 750','Plain 750','SGX 1024','Plain 1024','Location','SouthEast','Orientation','vertical')  
ylabel('Average Delay [\mu s]');
set(gca, 'xticklabel',{'100 \mu s';'1000 \mu s';'10000 \mu s'});
%print('../images/OVS_delay_already_existing_rule.pdf','-dpdf');

x=[1:10000];

figure
plot(x,sf*s2,'.', x,sf*v2,'x')
xlabel('Packet N#');ylabel('Delay [\mu s]');legend('SGX','Plain')
print('../images/OVS_instability.pdf','-dpdf');


figure
plot(x,sf*s2a,'.', x,sf*v2a,'x')
xlabel('Packet N#');ylabel('Delay [\mu s]');legend('SGX','Plain')
print('../images/OVS_stability.pdf','-dpdf');


