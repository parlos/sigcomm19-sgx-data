# sigcom19-sgx-data

Requirements (Ubuntu , adapt accordingly to your environment)

These are the packages that need to be installed;

sudo apt install build-essential git autoconf pkg-config libtool automake libpcap-dev libssl-dev



Then we start by cloning and installing the libcap_utils library. It will allow us to work with the trace files. 


#libcaputils
git clone https://github.com/DPMI/libcap_utils.git
cd libcap_utils
autoreconf -si
mkdir build; cd build
../configure
make
sudo make install

# return to starting point (assuming home directory)
cd 
git clone https://github.com/DPMI/consumer-onewaydelay.git
cd consumer-onewaydelay
make
sudo make install
cd 

Now we have the installed software, and we can use acceess the traces. 

do-processing.sh read the traces and creates the text files we import into matlab (v 2018b).



